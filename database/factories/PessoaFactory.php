<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PessoaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'tipo' => 'F',
            'matricula' => Str::random(7),
            'nome' => fake()->name(),
            'cpf' => Str::random(11),
            'email' => fake()->unique()->safeemail(),
            'telefone' => fake()->phoneNumber(),


        ];
    }
}
