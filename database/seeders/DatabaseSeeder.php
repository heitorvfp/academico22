<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // \App\Models\User::factory()->create([

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' =   > 'test@example.com',
        // ]);

        \App\Models\Pessoa::factory()->create([
            'nome' => 'Aluno teste',
            'tipo' => 'A',
        ]);
        \App\Models\Pessoa::factory()->create([
            'nome' => 'Professor teste',
            'tipo' => 'P',
        ]);
        \App\Models\Pessoa::factory()->create([
            'nome' => 'Funcionario teste',
            'tipo' => 'F',
        ]);
    }
}
